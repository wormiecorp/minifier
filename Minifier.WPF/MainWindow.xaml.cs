﻿// ***********************************************************************
//// Assembly         : Minifier.WPF
//// Author           : Kim Nordmo <kim.nordmo@gmail.com>
//// Created          : 2014-02-03
////
//// Last Modified By : Kim Nordmo <kim.nordmo@gmail.com>
//// Last Modified On : 2014-02-03
//// ***********************************************************************
// <copyright file="MainWindow.xaml.cs">
////     Copyright 2014 (c) Kim Nordmo.
////
////     This program is free software: you can redistribute it and/or modify
////     it under the terms of the GNU General Public License as published by
////     the Free Software Foundation, either version 3 of the License, or
////     (at your option) any later version.
////
////     This program is distributed in the hope that it will be useful,
////     but WITHOUT ANY WARRANTY; without event the implied warranty of
////     MERCHANTABLITY or FITNESS FOR A PARTICLUAR PURPOSE. See the
////     GNU General Public License for more details.
////
////     You should have received a copy of the GNU General Public License
////     along with this program. If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <summary></summary>
// ***********************************************************************
namespace Minifier.WPF
{
    #region Using Directives

    using System;
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;
    using System.Windows;
    using System.Windows.Forms;
    using System.Windows.Threading;
    using Minifier.Tools;
    using MessageBox = System.Windows.MessageBox;

    #endregion

    /// <summary>
    /// Interaction logic for MainWindow
    /// </summary>
    public partial class MainWindow
    {
        /// <summary>
        /// The _saved
        /// </summary>
        private int saved;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainWindow"/> class.
        /// </summary>
        public MainWindow()
        {
            this.InitializeComponent();
            this.TxtFolder.Text = AppDomain.CurrentDomain.BaseDirectory;
        }

        #region Nested type: EmptyDelegate

        /// <summary>
        /// Delegate EmptyDelegate
        /// </summary>
        private delegate void EmptyDelegate();

        #endregion

        /// <summary>
        /// Does the events.
        /// </summary>
        private static void doEvents()
        {
            Dispatcher.CurrentDispatcher.Invoke(DispatcherPriority.Background, new EmptyDelegate(delegate { }));
        }

        /// <summary>
        /// Handles the Click event of the Browse control.
        /// </summary>
        /// <param name="sender">
        /// The source of the event.
        /// </param>
        /// <param name="e">
        /// The <see cref="RoutedEventArgs"/> instance containing the event data.
        /// </param>
        [SuppressMessage("StyleCopPlus.StyleCopPlusRules", "SP0100:AdvancedNamingRules",
            Justification = "Reviewed. Suppression is OK here.")]
        private void buttonBrowseClick(object sender, RoutedEventArgs e)
        {
            var folderBrowserDialog = new FolderBrowserDialog { SelectedPath = this.TxtFolder.Text };
            folderBrowserDialog.ShowDialog();
            this.TxtFolder.Text = folderBrowserDialog.SelectedPath;
        }

        /// <summary>
        /// Handles the Click event of the Minify button control.
        /// </summary>
        /// <param name="sender">
        /// The source of the event.
        /// </param>
        /// <param name="e">
        /// The <see cref="RoutedEventArgs"/> instance containing the event data.
        /// </param>
        [SuppressMessage("StyleCopPlus.StyleCopPlusRules", "SP0100:AdvancedNamingRules",
            Justification = "Reviewed. Suppression is OK here.")]
        private void btn_minify_Click(object sender, RoutedEventArgs e)
        {
            this.saved = 0;
            if (this.TxtFolder.Text.Length < 10)
            {
                if (MessageBox.Show("Are you sure? Looks dangerous", "Danger Danger", MessageBoxButton.YesNo) !=
                    MessageBoxResult.Yes)
                {
                    return;
                }
            }

            this.BtnMinify.IsEnabled = false;
            this.BtnBrowse.IsEnabled = false;
            this.TxtFolder.IsEnabled = false;
            this.ChkBackup.IsEnabled = false;

            this.execute_compression("*.js");
            this.execute_compression("*.css");
            this.execute_compression("*.aspx", "*.master", "*.ascx", "*.html", "*.htm", "*.skin", "*.cshtml");

            this.LblStatus.Content = "Done";
            MessageBox.Show("Done! Saved " + this.saved.ToString("0,0") + " bytes");

            this.BtnMinify.IsEnabled = true;
            this.BtnBrowse.IsEnabled = true;
            this.TxtFolder.IsEnabled = true;
            this.ChkBackup.IsEnabled = true;
        }

        /// <summary>
        /// Execute_compressions the specified extensions.
        /// </summary>
        /// <param name="extensions">
        /// The extensions.
        /// </param>
        [SuppressMessage("StyleCopPlus.StyleCopPlusRules", "SP0100:AdvancedNamingRules",
            Justification = "Reviewed. Suppression is OK here.")]
        private void execute_compression(params string[] extensions)
        {
            var files = new List<string>();

            this.LblStatus.Content = "Collecting all " + string.Join(" ", extensions) + " files";
            doEvents();

            foreach (string ext in extensions)
            {
                Utilities.GetFiles(this.TxtFolder.Text, files, ext);
            }

            this.LblStatus.Content = "Minifying " + files.Count + " " + string.Join(" ", extensions) + " files";
            doEvents();

            this.ProgressBar1.Value = 0;
            this.ProgressBar1.Maximum = files.Count;

            var mini = new Minifier
                       {
                           // ReSharper disable once PossibleInvalidOperationException
                           Backup = this.ChkBackup.IsChecked.Value
                       };
            for (int i = 0; i < files.Count && this.Visibility == Visibility.Visible; i++)
            {
                this.saved += mini.Minify(files[i]);
                this.ProgressBar1.Value = this.ProgressBar1.Value + 1;
                doEvents();
            }
        }
    }
}
