﻿// ***********************************************************************
//// Assembly         : Minifier.WPF
//// Author           : Kim Nordmo <kim.nordmo@gmail.com>
//// Created          : 2014-02-03
////
//// Last Modified By : Kim Nordmo <kim.nordmo@gmail.com>
//// Last Modified On : 2014-02-03
//// ***********************************************************************
// <copyright file="App.xaml.cs">
////     Copyright 2014 (c) Kim Nordmo.
////
////     This program is free software: you can redistribute it and/or modify
////     it under the terms of the GNU General Public License as published by
////     the Free Software Foundation, either version 3 of the License, or
////     (at your option) any later version.
////
////     This program is distributed in the hope that it will be useful,
////     but WITHOUT ANY WARRANTY; without event the implied warranty of
////     MERCHANTABLITY or FITNESS FOR A PARTICLUAR PURPOSE. See the
////     GNU General Public License for more details.
////
////     You should have received a copy of the GNU General Public License
////     along with this program. If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <summary></summary>
// ***********************************************************************
namespace Minifier.WPF
{
    #region Using Directives

    using System.Windows;

    #endregion

    /// <summary>
    /// Interaction logic for App
    /// </summary>
    public partial class App
    {
    }
}
