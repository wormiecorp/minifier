﻿// ***********************************************************************
//// Assembly         : Minifier
//// Author           : Kim Nordmo <kim.nordmo@gmail.com>
//// Created          : 2014-02-03
////
//// Last Modified By : Kim Nordmo <kim.nordmo@gmail.com>
//// Last Modified On : 2014-02-03
//// ***********************************************************************
// <copyright file="Utilities.cs">
////     Copyright 2014 (c) Kim Nordmo.
////
////     This program is free software: you can redistribute it and/or modify
////     it under the terms of the GNU General Public License as published by
////     the Free Software Foundation, either version 3 of the License, or
////     (at your option) any later version.
////
////     This program is distributed in the hope that it will be useful,
////     but WITHOUT ANY WARRANTY; without event the implied warranty of
////     MERCHANTABLITY or FITNESS FOR A PARTICLUAR PURPOSE. See the
////     GNU General Public License for more details.
////
////     You should have received a copy of the GNU General Public License
////     along with this program. If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <summary></summary>
// ***********************************************************************
namespace Minifier.Tools
{
    #region Using Directives

    using System.Collections.Generic;
    using System.IO;

    #endregion

    /// <summary>
    /// The Utilities class.
    /// </summary>
    public static class Utilities
    {
        /// <summary>
        /// Gets the files.
        /// </summary>
        /// <param name="path">
        /// The path.
        /// </param>
        /// <param name="files">
        /// The files.
        /// </param>
        /// <param name="type">
        /// The type.
        /// </param>
        public static void GetFiles(string path, List<string> files, string type)
        {
            string[] filenames = Directory.GetFiles(path, type, SearchOption.AllDirectories);
            files.AddRange(filenames);
        }
    }
}
