﻿// ***********************************************************************
//// Assembly         : Minifier
//// Author           : Kim Nordmo <kim.nordmo@gmail.com>
//// Created          : 2014-02-03
////
//// Last Modified By : Kim Nordmo <kim.nordmo@gmail.com>
//// Last Modified On : 2014-02-03
//// ***********************************************************************
// <copyright file="Minifier.cs">
////     Copyright 2014 (c) Kim Nordmo.
////
////     This program is free software: you can redistribute it and/or modify
////     it under the terms of the GNU General Public License as published by
////     the Free Software Foundation, either version 3 of the License, or
////     (at your option) any later version.
////
////     This program is distributed in the hope that it will be useful,
////     but WITHOUT ANY WARRANTY; without event the implied warranty of
////     MERCHANTABLITY or FITNESS FOR A PARTICLUAR PURPOSE. See the
////     GNU General Public License for more details.
////
////     You should have received a copy of the GNU General Public License
////     along with this program. If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <summary></summary>
// ***********************************************************************
namespace Minifier.Tools
{
    #region Using Directives

    using System;
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;
    using System.IO;
    using System.Linq;
    using System.Text.RegularExpressions;
    using Microsoft.Ajax.Utilities;

    #endregion

    /// <summary>
    /// The Minifier class.
    /// </summary>
    public class Minifier
    {
        /// <summary>
        /// The compressors
        /// </summary>
        private readonly Dictionary<string, Func<string, string>> compressors;

        /// <summary>
        /// Initializes a new instance of the <see cref="Minifier"/> class.
        /// </summary>
        public Minifier()
        {
            this.compressors = new Dictionary<string, Func<string, string>>();
            this.compressors["js"] = minifyJavascript;
            this.compressors["css"] = this.minify_css;

            this.compressors["aspx"] = this.minify_aspnet;
            this.compressors["master"] = this.minify_aspnet;
            this.compressors["ascx"] = this.minify_aspnet;
            this.compressors["html"] = this.minify_aspnet;
            this.compressors["htm"] = this.minify_aspnet;
            this.compressors["skin"] = this.minify_aspnet;
            this.compressors["cshtml"] = this.minify_aspnet;
        }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="Minifier"/> should backup files before compressing them.
        /// </summary>
        /// <value><c>true</c> if backup; otherwise, <c>false</c>.</value>
        // ReSharper disable once MemberCanBePrivate.Global
        public bool Backup { get; set; }

        /// <summary>
        /// Minifies a file given its file path
        /// </summary>
        /// <param name="file">
        /// The file.
        /// </param>
        /// <returns>
        /// Bytes saved
        /// </returns>
        public int Minify(string file)
        {
            StreamReader sr;
            if (this.Backup)
            {
                File.Delete(file + ".backup");
                File.Move(file, file + ".backup");
                sr = new StreamReader(file + ".backup");
            }
            else
            {
                sr = new StreamReader(file);
            }

            string data = sr.ReadToEnd();
            string extension = file.Split('.').Last();
            int initialSize = data.Length;
            if (this.compressors.ContainsKey(extension))
            {
                data = this.compressors[extension](data);
            }

            sr.Close();

            var sw = new StreamWriter(file);
            sw.Write(data);
            sw.Close();

            return initialSize - data.Length;
        }

        /// <summary>
        /// Compress javascript using Ajax javascript compressor
        /// </summary>
        /// <param name="data">
        /// The data.
        /// </param>
        /// <returns>
        /// The compressed javascript.
        /// </returns>
        private static string minifyJavascript(string data)
        {
            try
            {
                var settings = new CodeSettings
                               {
                                   AllowEmbeddedAspNetBlocks = true,
                                   MinifyCode = true,
                                   RemoveUnneededCode = true,
                                   OutputMode = OutputMode.SingleLine
                               };
                var mini = new Microsoft.Ajax.Utilities.Minifier();

                return mini.MinifyJavaScript(data, settings);
            }
                // ReSharper disable once EmptyGeneralCatchClause
            catch
            {
            }

            return data;
        }

        /// <summary>
        /// Compress css using Ajax Css Compressor
        /// </summary>
        /// <param name="data">
        /// The data.
        /// </param>
        /// <returns>
        /// The compressed CSS.
        /// </returns>
        [SuppressMessage("StyleCopPlus.StyleCopPlusRules", "SP0100:AdvancedNamingRules",
            Justification = "Reviewed. Suppression is OK here."),
         SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1650:ElementDocumentationMustBeSpelledCorrectly",
             Justification = "Reviewed. Suppression is OK here.")]
        private string minify_css(string data)
        {
            try
            {
                var settings = new CssSettings
                               {
                                   AllowEmbeddedAspNetBlocks = true,
                                   CommentMode = CssComment.Hacks,
                                   MinifyExpressions = true,
                                   OutputMode = OutputMode.SingleLine
                               };

                var mini = new Microsoft.Ajax.Utilities.Minifier();
                return mini.MinifyStyleSheet(data, settings);
            }
                // ReSharper disable once EmptyGeneralCatchClause
            catch
            {
            }

            return data;
        }

        /// <summary>
        /// Minify the specified HTML.
        /// </summary>
        /// <param name="html">
        /// The HTML.
        /// </param>
        /// <returns>
        /// The minified HTML.
        /// </returns>
        [SuppressMessage("StyleCopPlus.StyleCopPlusRules", "SP0100:AdvancedNamingRules",
            Justification = "Reviewed. Suppression is OK here.")]
        private string minify_aspnet(string html)
        {
            // Minify inline javascript & css
            var tags = new[]
                       {
                           new
                           {
                               start =
                               new[]
                               {
                                   "<script type=\"text/javascript\">",
                                   "<script>",
                                   "<script language=\"javascript\">"
                               },
                               end = "</script>",
                               func = new Func<string, string>(minifyJavascript)
                           },
                           new
                           {
                               start = new[] { "<style type=\"text/css\">", "<style>" },
                               end = "</style>",
                               func = new Func<string, string>(this.minify_css)
                           }
                       };

            // Pointers to the string
            int endIndex = 0;
            while (true)
            {
                int currentIndex = endIndex;
                string currentTagKey = string.Empty;

                // Specifies the current tag object were working with
                var currentTag = tags[0];

                // Go through each tag and try to find it in the html, see which tag is closest to the currentIndex
                int newIndex = -1;
                foreach (var tag in tags)
                {
                    foreach (string tagkey in tag.start)
                    {
                        int index = html.IndexOf(tagkey, currentIndex, StringComparison.CurrentCultureIgnoreCase);
                        if (index != -1 &&
                            (newIndex == -1 || index < newIndex))
                        {
                            newIndex = index;
                            currentTagKey = tagkey;
                            currentTag = tag;
                        }
                    }
                }

                currentIndex = newIndex;

                // Whenever we're not in a script/style tag, we're in pure HTML mode.
                // Remove all empty spaces between 2 tags, but only do it within html.
                if (currentIndex == -1 ||
                    endIndex < currentIndex)
                {
                    if (currentIndex == -1)
                    {
                        currentIndex = html.Length;
                    }

                    // Take out the html code we want to minify
                    string newHtml = html.Substring(endIndex, currentIndex - endIndex);

                    // Use simple regex to remove all extra white spaces between tag (its not a smart one)
                    newHtml = Regex.Replace(newHtml, @"\s+<", " <", RegexOptions.Singleline);
                    newHtml = Regex.Replace(newHtml, @">\s+", "> ", RegexOptions.Singleline);

                    // Replace the existing html code with the new one
                    html = html.Remove(endIndex, currentIndex - endIndex);
                    html = html.Insert(endIndex, newHtml);

                    // Move the pointer to the end of this new html code
                    currentIndex = endIndex + newHtml.Length;

                    // If we reached the end of the whole html, exit
                    if (currentIndex == html.Length)
                    {
                        break;
                    }
                }

                // If we didn't find any more tags to work with, exit
                if (currentIndex == -1)
                {
                    break;
                }

                // Find the end tag
                endIndex = html.IndexOf(currentTag.end, currentIndex, StringComparison.CurrentCultureIgnoreCase);
                if (endIndex == -1)
                {
                    break;
                }

                // Take out the script/style code
                string script = html.Substring(
                    currentIndex + currentTagKey.Length,
                    endIndex - currentIndex - currentTagKey.Length);

                // Check if the script/style includes any server side code, that will make things messy, better avoid
                if (script.Contains("<%"))
                {
                    continue;
                }

                // Generate the compressed script and insert it into the html.
                string newScript = currentTag.func(script);
                html = html.Remove(currentIndex + currentTagKey.Length, endIndex - currentIndex - currentTagKey.Length);
                html = html.Insert(currentIndex + currentTagKey.Length, newScript);

                // Move the end pointer to the end of the </script> tag
                endIndex = currentIndex + currentTagKey.Length + newScript.Length + currentTag.end.Length;
            }

            return html;
        }
    }
}
