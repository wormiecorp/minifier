#define MyAppName "AspMinifier"
#define MyAppVersion "1.0.0.1"

[Setup]
WizardImageFile=SetupModern20.bmp
WizardSmallImageFile=SetupModernSmall19.bmp
ShowLanguageDialog=auto
AlwaysShowGroupOnReadyPage=True
AlwaysShowDirOnReadyPage=True
AppName={#MyAppName}
AppVersion={#MyAppVersion}
AppCopyright=Copyright (C) Kim Nordmo 2014
AppId={{9928EE38-7082-4E6E-AA40-0B0BB7A7AD8C}
LicenseFile=..\gplv3.txt
SetupIconFile=SetupModernSmall19.ico
DefaultDirName={pf}\{#MyAppName}
UninstallDisplayIcon={app}\Minifier.WPF.exe
ArchitecturesInstallIn64BitMode=x64
SolidCompression=True
Compression=lzma2/ultra64
InternalCompressLevel=ultra
DisableProgramGroupPage=auto
AllowNoIcons=True
AlwaysUsePersonalGroup=True
DefaultGroupName={#MyAppName}
DisableDirPage=auto
OutputBaseFilename={#MyAppName}-{#MyAppVersion}-Setup
MinVersion=0,5.01sp3

[Files]
Source: ".\isxdl.dll"; DestDir: "{app}"; Flags: dontcopy

Source: "..\Minifier.WPF\bin\Release\AjaxMin.dll"; DestDir: "{app}"; Components: minifier\core
Source: "..\Minifier.WPF\bin\Release\Minifier.dll"; DestDir: "{app}"; Components: minifier\core
Source: "..\Minifier.WPF\bin\Release\Minifier.WPF.exe"; DestDir: "{app}"; Components: minifier\core

[Types]
Name: "recommended"; Description: "Recommended"
Name: "full"; Description: "Full"
Name: "custom"; Description: "Custom"; Flags: iscustom

[Tasks]
Name: "desktopicon"; Description: "{cm:CreateDesktopIcon}"; Flags: unchecked

[Components]
Name: "minifier"; Description: "{#MyAppName}"
Name: "minifier\core"; Description: "Core Files"; Types: full recommended custom; Flags: fixed

[Icons]
Name: "{userdesktop}\{#MyAppName}"; Filename: "{app}\Minifier.WPF.exe"; WorkingDir: "{app}"; IconFilename: "{app}\Minifier.WPF.exe"; Components: minifier; Tasks: desktopicon
Name: "{group}\{#MyAppName}"; Filename: "{app}\Minifier.WPF.exe"; WorkingDir: "{app}"; IconFilename: "{app}\Minifier.WPF.exe"; Components: minifier
Name: "{group}\{cm:UninstallProgram, {#MyAppName}}"; Filename: "{uninstallexe}"; IconFilename: "SetupModernSmall19.ico"

[Run]
Filename: "{app}\Minifier.WPF.exe"; WorkingDir: "{app}"; Flags: nowait postinstall runascurrentuser unchecked skipifsilent; Description: "Run {#MyAppName}"; Components: minifier\core

[Code]
var
  dotnetRedistPath: string;
  downloadNeeded: boolean;
  dotNetNeeded: boolean;
  memoDependenciesNeeded: string;
  
procedure isxdl_AddFile(URL, Filename: PAnsiChar);
external 'isxdl_AddFile@files:isxdl.dll stdcall';
function isxdl_DownloadFiles(hWnd: Integer): Integer;
external 'isxdl_DownloadFiles@files:isxdl.dll stdcall';
function isxdl_SetOption(Option, Value: PAnsiChar): Integer;
external 'isxdl_SetOption@files:isxdl.dll stdcall';

const
  dotnetRedistURL = 'http://download.microsoft.com/download/7/B/6/7B629E05-399A-4A92-B5BC-484C74B5124B/dotNetFx40_Client_setup.exe';
  
const
  dotnetRedistKey =
  'SOFTWARE\Microsoft\.NETFramework\Policy\v4.0';

function InitializeSetup(): Boolean;

begin
  Result := true;
  dotNetNeeded := false;
  
  if (not RegKeyExists(HKLM, dotnetRedistKey)) then begin
    dotNetNeeded := true;
    if (not IsAdminLoggedOn()) then begin
      MsgBox('{#MyAppName} needs the Microsoft .NET 4.0 Client Framework to be installed by an Administrator', mbInformation, MB_OK)
      Result := false;
    end else begin
      memoDependenciesNeeded := memoDependenciesNeeded + '      .NET 4.0 Client Profile' #13;
      dotnetRedistPath := ExpandConstant('{src}\dotnetfx.exe');
      if not FileExists(dotnetRedistPath) then begin
        dotnetRedistPath := ExpandConstant('{tmp}\dotnetfx.exe');
        if not FileExists(dotnetRedistPath) then begin
          isxdl_AddFile(dotnetRedistURL, dotnetRedistPath);
          downloadNeeded := true;
        end;
      end;
      SetIniString('install', 'dotnetRedist', dotnetRedistPath, ExpandConstant('{tmp}\dep.ini'));
    end;
  end;
  
end;

function NextButtonClick(CurPage: Integer): Boolean;
var
  hWnd: Integer;
  ResultCode: Integer;
  sModuleName: String;
  nCode: Integer;
begin
  Result := true;

  if CurPage = wpReady then begin

    hWnd := StrToInt(ExpandConstant('{wizardhwnd}'));

    if downloadNeeded then begin

      isxdl_SetOption('label', 'Downloading Microsoft .NET 4.0 Framework');
      isxdl_SetOption('description', '{#MyAppName} needs to install the Microsoft .NET 4.0 Framework. Please wait while Setup is downloading extra files to your computer.');
      if isxdl_DownloadFiles(hWnd) = 0 then Result := false;
    end;
    if (Result = true) and (dotNetNeeded = true) then begin
      if Exec(ExpandConstant(dotnetRedistPath), '/passive', '', SW_SHOW, ewWaitUntilTerminated, ResultCode) then begin
         if not (ResultCode = 0) then begin
           Result := false;
         end;
      end else begin
         Result := false;
      end;
    end;
  end;
end;

function UpdateReadyMemo(Space, NewLine, MemoUserInfoInfo, MemoDirInfo, MemoTypeInfo, MemoComponentsInfo, MemoGroupInfo, MemoTasksInfo: String): String;
var
  s: string;

begin
  if memoDependenciesNeeded <> '' then s := s + 'Dependencies to install:' + NewLine + memoDependenciesNeeded + NewLine;
  if MemoDirInfo <> '' then s := s + MemoDirInfo + NewLine + NewLine;
  if MemoGroupInfo <> '' then s := s + MemoGroupInfo + NewLine + NewLine;
  if MemoTasksInfo <> '' then s := s + MemoTasksInfo + NewLine + NewLine;

  Result := s
end;
